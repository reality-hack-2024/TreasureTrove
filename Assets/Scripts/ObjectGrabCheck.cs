using UnityEngine;
using Oculus.Interaction.GrabAPI;
using Normal.Realtime;

public class ObjectGrabCheck : MonoBehaviour
{
    public HandGrabAPI handGrabAPIL;
    public HandGrabAPI handGrabAPIR;

    public bool m_isGrabbed;
    public bool m_triggerEvent;

    public string m_prefab;
    public GameObject playerRig;

    private void Start()
    {
        Debug.Log("onStart()");
        playerRig = GameObject.Find("Player Rig");
        handGrabAPIL = playerRig.transform.GetChild(1).GetChild(1).GetChild(0).GetChild(3).GetChild(0).GetComponent<HandGrabAPI>();
        handGrabAPIR = playerRig.transform.GetChild(1).GetChild(1).GetChild(1).GetChild(3).GetChild(0).GetComponent<HandGrabAPI>();


        if (handGrabAPIL != null && handGrabAPIR != null)
        {
            Debug.LogWarning("No HandGrabAPI component found in the scene.");
        }
    }
   
    private void Update()
    {
        if (handGrabAPIL != null && handGrabAPIR != null)
        {
            if (handGrabAPIL.IsHandPinchGrabbing(GrabbingRule.DefaultPinchRule) || handGrabAPIR.IsHandPinchGrabbing(GrabbingRule.DefaultPinchRule) || handGrabAPIL.IsHandPalmGrabbing(GrabbingRule.DefaultPalmRule) || handGrabAPIR.IsHandPalmGrabbing(GrabbingRule.DefaultPalmRule))
            {
                m_isGrabbed = true;
            }
            else
            {
                m_isGrabbed = false;
            }
        }
        if (m_triggerEvent)
        {
            Realtime.Instantiate(m_prefab);
            m_triggerEvent = false;
        }
    }
}