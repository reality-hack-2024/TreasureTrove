using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class TextToSpeech : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;

    // Call this method with the path of the recorded audio file to play it
    public void PlayRecordedAudio(string audioFilePath)
    {
        StartCoroutine(PlayRecordedAudioCoroutine(audioFilePath));
    }

    private IEnumerator PlayRecordedAudioCoroutine(string audioFilePath)
    {
        string audioPath = "file://" + Path.Combine(Application.persistentDataPath, audioFilePath);
        using (var www = UnityWebRequestMultimedia.GetAudioClip(audioPath, DetermineAudioType(audioFilePath)))
        {
            yield return www.SendWebRequest(); // Unity's coroutine system will wait for the request to complete

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError($"Failed to load audio clip: {www.error}");
            }
            else
            {
                // Set the downloaded audio clip to the audio source and play it
                audioSource.clip = DownloadHandlerAudioClip.GetContent(www);
                audioSource.Play();
            }
        }
    }

    // This helper method determines the AudioType based on the file extension
    private AudioType DetermineAudioType(string audioFilePath)
    {
        string extension = Path.GetExtension(audioFilePath).ToLower();
        switch (extension)
        {
            case ".mp3":
                return AudioType.MPEG;
            case ".wav":
                return AudioType.WAV;
            case ".ogg":
                return AudioType.OGGVORBIS;
            // Add other cases for different audio types if needed
            default:
                Debug.LogError("Unsupported audio file type.");
                return AudioType.UNKNOWN;
        }
    }
}
