﻿using System;
using System.IO; ///
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Normal.Realtime;
//using Microsoft.MixedReality.Toolkit.UI;

namespace OpenAI
{
    public class Whisper : MonoBehaviour
    {
        //[SerializeField] private Button recordButton;
        [SerializeField] private Dropdown dropdown;
        //[SerializeField] private ChatTest chatTest;
        //[SerializeField] private Image progress;
        //public TextMeshProUGUI transcriptionText;


        private readonly string fileName = "output.wav";
        private readonly int duration = 20;

        private AudioClip clip;
        public bool isRecording;
        private float time;
        private OpenAIApi openai = new OpenAIApi("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");

        public int index;
        public string m_totemPrefab;

        private void Start()
        {
            foreach (var device in Microphone.devices)
            {
                dropdown.options.Add(new Dropdown.OptionData(device));
            }
            //recordButton.onClick.AddListener(StartRecording);
            dropdown.onValueChanged.AddListener(ChangeMicrophone);

            //var index = PlayerPrefs.GetInt("user-mic-device-index");
            dropdown.SetValueWithoutNotify(index);
        }


        private void ChangeMicrophone(int index)
        {
            PlayerPrefs.SetInt("user-mic-device-index", index);
        }

        public async void StartRecording()
        {
            if (isRecording) // if is recording -> end recording -> generate totem and link to the saved wav
            {
                isRecording = false;
                Debug.Log("Stop recording...");

                Microphone.End(null);
                byte[] data = SaveWav.Save(fileName, clip);

                // Save the audio data to a file
                string filePath = Path.Combine(Application.persistentDataPath, fileName);
                File.WriteAllBytes(filePath, data);
                Debug.Log("Saved recording to " + filePath);

                // save to persistent data path
                var req = new CreateAudioTranscriptionsRequest
                {
                    FileData = new FileData() { Data = data, Name = "audio.wav" },
                    // File = Application.persistentDataPath + "/" + fileName,
                    Model = "whisper-1",
                    Language = "en"
                };
                var res = await openai.CreateAudioTranscription(req);


                //// instantiate totem
                //GameObject thisTotem = Realtime.Instantiate(m_totemPrefab);
                //// save audio clip to totem
                //thisTotem.GetComponent<totemHandler>().audioFilePath = filePath;

                //chatTest.SendReply(res.Text);
                //transcriptionText.text = res.Text;

            }
            else
            {
                Debug.Log("Start recording...");
                isRecording = true;

                var index = PlayerPrefs.GetInt("user-mic-device-index");
                clip = Microphone.Start(null, false, duration, 44100);
            }
        }

        private void Update()
        {
            if (isRecording)
            {
                time += Time.deltaTime;
                //progress.fillAmount = time / duration;
            }

            if (time >= duration)
            {
                time = 0;
                //progress.fillAmount = 0;
                StartRecording();
            }
        }
    }
}
