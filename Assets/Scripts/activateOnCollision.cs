using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenAI;

public class ActivateOnCollision : MonoBehaviour
{
    private bool alreadyUsed = false;
    public Color startColor = Color.green;
    public Color stopColor = Color.red;
    public Renderer targetRenderer;
    private Whisper m_whisperScript;
    public string childGameObjectName; // Name of the child GameObject to be activated/deactivated
    public string textChildGameObjectName; // Name of the child GameObject to be activated/deactivated

    private void Start()
    {
        alreadyUsed = false;
        targetRenderer = transform.GetChild(1).GetComponent<Renderer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger");

        if (other.CompareTag("table") && !alreadyUsed)
        {
            Debug.Log("Touched table. Start Recording");
            // if (!m_whisperScript.isRecording) m_whisperScript.StartRecording();

            if (targetRenderer != null)
            {
                targetRenderer.material.color = startColor;
            }

            // Activate child GameObject
            Transform childTransform = transform.Find(childGameObjectName);
            if (childTransform != null)
            {
                childTransform.gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("table") && !alreadyUsed)
        {
            Debug.Log("Left table. Stop Recording");
            // if (m_whisperScript.isRecording) m_whisperScript.StartRecording();

            if (targetRenderer != null)
            {
                targetRenderer.material.color = stopColor;
                targetRenderer.enabled = false; // Turn off the renderer

            }
            alreadyUsed = true;

            // Deactivate child GameObject
            Transform childTransform = transform.Find(childGameObjectName);
            Transform textChildTransform = transform.Find(textChildGameObjectName);
            if (childTransform != null)
            {
                childTransform.gameObject.SetActive(true);
                textChildTransform.gameObject.SetActive(false);

            }
        }
    }
}
