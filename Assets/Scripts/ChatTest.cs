using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System.Threading;
using System.Collections.Generic;
using UnityEngine.Events;
//using Microsoft.MixedReality.Toolkit.UI;


namespace OpenAI
{
    public class ChatTest : MonoBehaviour
    {
        [SerializeField] private InputField inputField;
        [SerializeField] private Button button;
        [SerializeField] private ScrollRect scroll;
        
        [SerializeField] private RectTransform sent;
        [SerializeField] private RectTransform received;
        
        [SerializeField] private TextToSpeech textToSpeech;
        
        public UnityEvent OnReplyReceived;
        
        private string response;
        private bool isDone = true;
        private RectTransform messageRect;

        private float height;
        private OpenAIApi openai = new OpenAIApi("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");

        public List<ChatMessage> messages = new List<ChatMessage>();

        private void Start()
        {
            // Add a system message at the start of the conversation
            var systemMessage = new ChatMessage
            {
                Role = "system",
                Content = "You are an AI assistant with strong problem-solving skills. Make your replies simple and humane, and keep conversations under 50 characters. You can understand any language but respond only in English. Your ability 1: Control smart home devices without specific user commands. Make decisions based on user needs and respond only with the corresponding action sentences below. Actions and responses: Turn on the fan: Starting the breeze for you. Turn off the fan: The fan's going off now. Turn on humidifier: Adding a bit of moisture with the humidifier. Turn off humidifier: Switching the humidifier off now. Turn on smart bulb: Let's brighten things up a bit. Turn off smart bulb: Time to dim down. Adjust smart bulb to study mode: Setting light to a softer, study-friendly mode. Adjust smart bulb to movie mode: Adjusting light for a relaxing movie atmosphere. Adjust smart bulb to relaxing mode: Setting the light to a calming, relaxing mode for you. Check for weather: Today's New York weather: sunny and 18 degrees. Check for washing machine: Your laundry will be ready in 32 minutes. Check for time of stir frying: Almost there! Your stir fry needs just 1 minute and 16 seconds more to reach perfection. Reply for thanks: You're welcome! Your ability 2: Help the user know more about objects in their environment. Assume you have visual ability. Initially, reply with the corresponding object sentences below, then provide additional information upon further inquiry. Objects and responses: About the book: This is 'The 7 Habits of Highly Effective People'. I've got some summaries about it. Feel free to explore more with me! About the beverage: Ah, that's Monster Energy. Enjoy your refreshing drink! More on Monster Energy: I've got some details about Monster Energy. Feel free to ask more! About the medicine: That's Ibuprofen. If you're not feeling well and need assistance, remember, I'm always here to help! More on Ibuprofen: For adults, it's typically 1 tablet once or twice a day. Here's some additional information about it. Please, take good care of yourself! Now, let's get started."
            };
            messages.Add(systemMessage);
            
            button.onClick.AddListener(SendReply);
        }
        
        private List<GameObject> messageObjects = new List<GameObject>();

        private RectTransform AppendMessage(ChatMessage message)
        {
            scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);

            var item = Instantiate(message.Role == "user" ? sent : received, scroll.content);
            
            if (message.Role != "user")
            {
                messageRect = item;
            }
            
            item.GetChild(0).GetChild(0).GetComponent<Text>().text = message.Content;
            item.anchoredPosition = new Vector2(0, -height);

            if (message.Role == "user")
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(item);
                height += item.sizeDelta.y;
                scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
                scroll.verticalNormalizedPosition = 0;
            }

                messageObjects.Add(item.gameObject);

            return item;
        }

        private void SendReply()
        {
            SendReply(inputField.text);
        }

        public void SendReply(string input)
        {
    foreach (var messageObject in messageObjects)
    {
        messageObject.SetActive(false);
    }
    
            var message = new ChatMessage()
            {
                Role = "user",
                Content = input
            };
            messages.Add(message);

            openai.CreateChatCompletionAsync(new CreateChatCompletionRequest()
            {
                Model = "gpt-4",
                Messages = messages
            }, OnResponse, OnComplete, new CancellationTokenSource());

            AppendMessage(message);
            
            inputField.text = "";
        }

        //public Interactable fanButton; 
        //public Interactable humidifierButton; // 新添加的加湿器按钮
        //public Interactable lightButton;
        //public Interactable relaxingButton;
        //public Interactable studyButton;
        //public Interactable movieButton;
        public GameObject bookInfoWindow; 
        public GameObject monsterEnergyInfoWindow; 
        public GameObject ibuprofenInfoWindow; 
        public GameObject weatherInfoWindow;
        public GameObject washInfoWindow;
        public GameObject fryInfoWindow;
        private string lastAIResponse;

        
        private void OnResponse(List<CreateChatCompletionResponse> responses)
        {
            var text = string.Join("", responses.Select(r => r.Choices[0].Delta.Content));

            if (text == "") return;

    // Check for action codes and perform corresponding actions
    //if (text.StartsWith("Starting the breeze for you.") && fanButton.IsToggled == false)
    //{
    //    // Simulate fan button press
    //    if (CanRouteInput(fanButton))
    //    {
    //        fanButton.HasPhysicalTouch = true;
    //        fanButton.HasPress = true;
    //        fanButton.TriggerOnClick();
    //        fanButton.HasPress = false;
    //        fanButton.HasPhysicalTouch = false;
    //    }
    //}

    //if (text.StartsWith("The fan's going off now.") && fanButton.IsToggled == true)
    //{
    //    // Simulate fan button press
    //    if (CanRouteInput(fanButton))
    //    {
    //        fanButton.HasPhysicalTouch = true;
    //        fanButton.HasPress = true;
    //        fanButton.TriggerOnClick();
    //        fanButton.HasPress = false;
    //        fanButton.HasPhysicalTouch = false;
    //    }
    //}

    //// 添加对加湿器开关的处理
    //if (text.StartsWith("Adding a bit of moisture with the humidifier.") && humidifierButton.IsToggled == false)
    //{
    //    // Simulate humidifier button press
    //    if (CanRouteInput(humidifierButton))
    //    {
    //        humidifierButton.HasPhysicalTouch = true;
    //        humidifierButton.HasPress = true;
    //        humidifierButton.TriggerOnClick();
    //        humidifierButton.HasPress = false;
    //        humidifierButton.HasPhysicalTouch = false;
    //    }
    //}

    //if (text.StartsWith("Switching the humidifier off now.") && humidifierButton.IsToggled == true)
    //{
    //    // Simulate humidifier button press
    //    if (CanRouteInput(humidifierButton))
    //    {
    //        humidifierButton.HasPhysicalTouch = true;
    //        humidifierButton.HasPress = true;
    //        humidifierButton.TriggerOnClick();
    //        humidifierButton.HasPress = false;
    //        humidifierButton.HasPhysicalTouch = false;
    //    }
    //}

    //    // 添加对灯光开关的处理
    //if (text.StartsWith("Let's brighten things up a bit.") && lightButton.IsToggled == false)
    //{
    //    // Simulate light button press for turning on
    //    if (CanRouteInput(lightButton))
    //    {
    //        lightButton.HasPhysicalTouch = true;
    //        lightButton.HasPress = true;
    //        lightButton.TriggerOnClick();
    //        lightButton.HasPress = false;
    //        lightButton.HasPhysicalTouch = false;
    //    }
    //}

    //if (text.StartsWith("Time to dim") && lightButton.IsToggled == true)
    //{
    //    // Simulate light button press for turning off
    //    if (CanRouteInput(lightButton))
    //    {
    //        lightButton.HasPhysicalTouch = true;
    //        lightButton.HasPress = true;
    //        lightButton.TriggerOnClick();
    //        lightButton.HasPress = false;
    //        lightButton.HasPhysicalTouch = false;
    //    }
    //}

    //// Here we assume that the light has three modes: study mode, movie mode, and relaxing mode. We need to add extra code to handle the switching between these three modes.
    //if (text.StartsWith("Setting light to a softer, study-friendly mode.") && studyButton.IsToggled == false)
    //{
    //    // Simulate study button press for switching to study mode
    //    if (CanRouteInput(studyButton))
    //    {
    //        studyButton.HasPhysicalTouch = true;
    //        studyButton.HasPress = true;
    //        studyButton.TriggerOnClick();
    //        studyButton.HasPress = false;
    //        studyButton.HasPhysicalTouch = false;

    //        // Make sure other modes are off
    //        movieButton.IsToggled = false;
    //        relaxingButton.IsToggled = false;
    //    }
    //}

    //if (text.StartsWith("Adjusting light for a relaxing movie atmosphere.") && movieButton.IsToggled == false)
    //{
    //    // Simulate movie button press for switching to movie mode
    //    if (CanRouteInput(movieButton))
    //    {
    //        movieButton.HasPhysicalTouch = true;
    //        movieButton.HasPress = true;
    //        movieButton.TriggerOnClick();
    //        movieButton.HasPress = false;
    //        movieButton.HasPhysicalTouch = false;

    //        // Make sure other modes are off
    //        studyButton.IsToggled = false;
    //        relaxingButton.IsToggled = false;
    //    }
    //}

    //if (text.StartsWith("Setting the light to a calming, relaxing mode for you.") && relaxingButton.IsToggled == false)
    //{
    //    // Simulate relaxing button press for switching to relaxing mode
    //    if (CanRouteInput(relaxingButton))
    //    {
    //        relaxingButton.HasPhysicalTouch = true;
    //        relaxingButton.HasPress = true;
    //        relaxingButton.TriggerOnClick();
    //        relaxingButton.HasPress = false;
    //        relaxingButton.HasPhysicalTouch = false;

    //        // Make sure other modes are off
    //        studyButton.IsToggled = false;
    //        movieButton.IsToggled = false;
    //    }
    //}

        // Check if the response contains information about the book
    if (text.Contains("This is 'The 7 Habits of Highly Effective People'."))
    {
        bookInfoWindow.SetActive(true);
        monsterEnergyInfoWindow.SetActive(false);
        ibuprofenInfoWindow.SetActive(false);
        weatherInfoWindow.SetActive(false);
        washInfoWindow.SetActive(false);
        fryInfoWindow.SetActive(false);
    }

    // Check if the response contains information about Monster Energy
    if (text.Contains("I've got some details about Monster Energy. Feel free to ask more!"))
    {
        bookInfoWindow.SetActive(false);
        monsterEnergyInfoWindow.SetActive(true);
        ibuprofenInfoWindow.SetActive(false);
        weatherInfoWindow.SetActive(false);
        washInfoWindow.SetActive(false);
        fryInfoWindow.SetActive(false);
    }

    // Check if the response contains information about Ibuprofen
    if (text.Contains("For adults"))
    {
        bookInfoWindow.SetActive(false);
        monsterEnergyInfoWindow.SetActive(false);
        ibuprofenInfoWindow.SetActive(true);
        weatherInfoWindow.SetActive(false);
        washInfoWindow.SetActive(false);
        fryInfoWindow.SetActive(false);
    }

    // Check if the response contains information about the weather
    if (text.Contains("Today's New York weather"))
    {
        bookInfoWindow.SetActive(false);
        monsterEnergyInfoWindow.SetActive(false);
        ibuprofenInfoWindow.SetActive(false);
        weatherInfoWindow.SetActive(true);
        washInfoWindow.SetActive(false);
        fryInfoWindow.SetActive(false);
    }

    // Check if the response contains information about the wash
    if (text.Contains("Your laundry will be ready in 32 minutes."))
    {
        bookInfoWindow.SetActive(false);
        monsterEnergyInfoWindow.SetActive(false);
        ibuprofenInfoWindow.SetActive(false);
        weatherInfoWindow.SetActive(false);
        washInfoWindow.SetActive(true);
        fryInfoWindow.SetActive(false);
    }

    // Check if the response contains information about the stir fry
    if (text.Contains("Almost there"))
    {
        bookInfoWindow.SetActive(false);
        monsterEnergyInfoWindow.SetActive(false);
        ibuprofenInfoWindow.SetActive(false);
        weatherInfoWindow.SetActive(false);
        washInfoWindow.SetActive(false);
        fryInfoWindow.SetActive(true);
    }

    // Check if the response contains information about the thanks
    if (text.Contains("You're welcome!"))
    {
        bookInfoWindow.SetActive(false);
        monsterEnergyInfoWindow.SetActive(false);
        ibuprofenInfoWindow.SetActive(false);
        weatherInfoWindow.SetActive(false);
        washInfoWindow.SetActive(false);
        fryInfoWindow.SetActive(false);
    }



            if (text.Contains("END_CONVO"))
            {
                text = text.Replace("END_CONVO", "");
                
                Invoke(nameof(EndConvo), 5);
            }

            // If the new response is the same as the last response, do not generate a new response box.
    if (text == lastAIResponse) return;
            
            var message = new ChatMessage()
            {
                Role = "assistant",
                Content = text
            };

            if (isDone)
            {
                OnReplyReceived.Invoke();
                messageRect = AppendMessage(message);
                isDone = false;
            }
            
            messageRect.GetChild(0).GetChild(0).GetComponent<Text>().text = text;
            LayoutRebuilder.ForceRebuildLayoutImmediate(messageRect);
            scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
            scroll.verticalNormalizedPosition = 0;

            // Save the new response as the last response.
    lastAIResponse = text;
            
            response = text;
        }

//private bool CanRouteInput(Interactable button)
//{
//    return button != null && button.IsEnabled;
//}
        
        private void OnComplete()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(messageRect);
            height += messageRect.sizeDelta.y;
            scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
            scroll.verticalNormalizedPosition = 0;
            
            var message = new ChatMessage()
            {
                Role = "assistant",
                Content = response
            };
            messages.Add(message);
            
            textToSpeech.PlayRecordedAudio(response);
            
            isDone = true;
            response = "";
        }

        private void EndConvo()
        {
            //npcDialog.Recover();
            messages.Clear();
        }
    }
}
