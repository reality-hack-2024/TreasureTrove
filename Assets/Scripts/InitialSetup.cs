using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Normal.Realtime;

public class InitialSetup : MonoBehaviour
{
    public GameObject m_player;
    public GameObject m_worldRoot;
    public Vector3 m_offset;

    //public string m_prefab;
    void Start()
    {
        m_worldRoot.transform.position = m_player.transform.position + m_offset;
        //m_moveable.GetComponent<RequestOwnership>().enabled = true;
        //Realtime.Instantiate(m_prefab);
    }

    
    void Update()
    {
        
    }
}
