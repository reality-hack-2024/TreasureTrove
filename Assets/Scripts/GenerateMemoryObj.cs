using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMemoryObj : MonoBehaviour
{
    public GameObject[] m_memoryObjs;
    public bool generateObj;
    public GameObject m_righthand;

    void Start()
    {
        
    }

    
    void Update()
    {
        if( generateObj){
            int rand = Random.Range(0, m_memoryObjs.Length);
            Instantiate(m_memoryObjs[rand], m_righthand.transform.position + new Vector3(0f, 0.1f,0f), Quaternion.identity);
            generateObj = !generateObj;
        }
    }
}
