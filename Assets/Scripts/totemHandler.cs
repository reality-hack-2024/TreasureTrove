using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class totemHandler : MonoBehaviour
{
    public AudioSource audioSource;
    public string audioFilePath;
    private bool buttonPushed;
    // bool buttonReleased = true;
    private ObjectGrabCheck handGrabComponent;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Debug.Log("started");
        buttonPushed = false;
        handGrabComponent = GetComponent<ObjectGrabCheck>();
    }

    //// Update is called once per frame
    //void Update()
    //{
    //    Debug.Log(handGrabComponent.m_isGrabbed);
    //    if (handGrabComponent.m_isGrabbed)
    //    {

    //            buttonPushed = true;
    //            Debug.Log("Pushed button" + buttonPushed.ToString());
    //            if (audioSource.clip == null)
    //            {
    //                linAudioClip();
    //            }
    //            audioSource.Play();
        
    //    }
    //    else
    //    {
    //        //Debug.Log("Released");

    //            buttonPushed = false;
    //            Debug.Log("Released button" + buttonPushed.ToString());
    //            audioSource.Pause();
        
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("hand"))
        {
            Debug.Log("triggered");
            if (!buttonPushed)
            {
                buttonPushed = true;
                Debug.Log("Pushed button" + buttonPushed.ToString());
                audioSource.Play();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("hand"))
        {
            Debug.Log("exit trigger");
            if (buttonPushed)
            {
                buttonPushed = false;
                Debug.Log("Released button" + buttonPushed.ToString());
                audioSource.Pause();
            }
        }
    }

    public void linAudioClip()
    {
        AudioClip audioClip = Resources.Load<AudioClip>(audioFilePath);
        audioSource.clip = audioClip;
    }

}
