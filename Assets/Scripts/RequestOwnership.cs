using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Normal.Realtime;

public class RequestOwnership : MonoBehaviour
{
    public RealtimeView m_realtimeView;
    public RealtimeTransform m_realtimeTransform;
    public ObjectGrabCheck m_grabcheckScript;
    //public string m_prefab;

    void Start()
    {
        
    }

    void Update()
    {
        if (m_grabcheckScript.m_isGrabbed)
        {
            m_realtimeView.RequestOwnership();
            m_realtimeTransform.RequestOwnership();
            //Realtime.Instantiate(m_prefab);
        }
    }
}
