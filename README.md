# Treasure Trove

Treasure Trove is...


## How to run:
- Connect Quest headset using Quest Link through a cable or air link.
- On any computer, open the project, go to the main scene, and click Play
- Your friend can join the same room by starting the project themselves
  - If you would like your own private room, create an account with normcore.io, set up your own room key, and send it to your friend to run.

## Usage:
- When you first enter the app, you will enter the room. Look on the far wall, and you can see a bulletin board of prompts
- This app is best run in a large room, as locomotion in the space is meant to be walkable, get some beanbags to make the virtual chairs real! You want to have really nice, long conversations sitting here.
- You and your partner (friend, parent, child, etc.) can pick any prompt speaks most to you. Pick one out and put it on the table
- The card will change color when set on the table, indicating it started the recording
- Once you are done talking, lift the card, and a 3D object will be generated, encapsulating your conversation
- Place this object anywhere you want in the room. As you have more conversations, your room fills up with memories
- Place your hand on a generated object at any time to hear the conversation again. 





